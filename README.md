VPC Peering Blueprint
=====================

This Terraform module is a blueprint for VPC peering and route configuration

# Use

To create a VPC Peering between two VPCs with this module you need to insert the following piece of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source  = "git::ssh://git@bitbucket.org/credibilit/terraform-vpc-peering-blueprint.git?ref=<VERSION>"
  account = "${var.account}"

  // VPCs
  vpc_id      = "${aws_vpc.requester.id}"
  peer_vpc_id = "${aws_vpc.accepter.id}"

  // Routes in requester to accepter
  vpc_route_tables      = "${list(
    aws_route_table.r_prv_a.id,
    aws_route_table.r_prv_b.id,
    aws_route_table.r_pub.id
  )}"
  vpc_route_table_count = 3

  // Routes in accepter to requester
  peer_route_tables       = "${list(
    aws_route_table.a_prv_a.id,
    aws_route_table.a_prv_b.id,
    aws_route_table.a_pub.id
  )}"
  peer_route_table_count  = 3
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.


## Input parameters

The following parameters are used on this module:

- `account`: The AWS account number ID

- `vpc_id`: The ID of the requester VPC
- `peer_vpc_id`: The ID of the VPC with which you are creating the VPC Peering Connection

- `vpc_route_tables`: List of ID of routing tables to add routes to vpc peering conn
- `vpc_route_table_count`: Count of vpc_route_tables
- `peer_route_tables`: List of ID of routing tables to add routes to vpc peering conn
- `peer_route_table_count`: Count of peer_route_tables"


## Output parameters

This are the outputs exposed by this module.

- `routes_in_requester_to_accepter`
- `routes_in_accepter_to_requester`
- `peering`
