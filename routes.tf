// Route for Requester VPC (local) to Accepter VPC
resource "aws_route" "vpc" {
  route_table_id            = "${element(var.vpc_route_tables, count.index)}"
  destination_cidr_block    = "${data.aws_vpc.accepter.cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peer.id}"

  count = "${var.enable ? var.vpc_route_table_count : 0}"
}

output "routes_in_requester_to_accepter" {
  value = {
    route_table_id            = ["${aws_route.vpc.*.route_table_id}"]
    destination_cidr_block    = ["${aws_route.vpc.*.destination_cidr_block}"]
    vpc_peering_connection_id = ["${aws_route.vpc.*.vpc_peering_connection_id}"]
  }
}


// Route for Accepter VPC to Requester VPC (local)
resource "aws_route" "peer" {
  route_table_id            = "${element(var.peer_route_tables, count.index)}"
  destination_cidr_block    = "${data.aws_vpc.requester.cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peer.id}"

  count = "${var.enable ? var.peer_route_table_count : 0}"
}

output "routes_in_accepter_to_requester" {
  value = {
    route_table_id            = ["${aws_route.peer.*.route_table_id}"]
    destination_cidr_block    = ["${aws_route.peer.*.destination_cidr_block}"]
    vpc_peering_connection_id = ["${aws_route.peer.*.vpc_peering_connection_id}"]
  }
}
