resource "aws_vpc_peering_connection" "peer" {
  vpc_id        = "${data.aws_vpc.requester.id}"
  peer_vpc_id   = "${data.aws_vpc.accepter.id}"
  peer_owner_id = "${var.account}"

  auto_accept   = true
  accepter {
    allow_remote_vpc_dns_resolution = true
  }
  requester {
    allow_remote_vpc_dns_resolution = true
  }

  tags {
    Name = "${lookup(data.aws_vpc.requester.tags, "Name")} (${data.aws_vpc.requester.id}) and ${lookup(data.aws_vpc.accepter.tags, "Name")} (${data.aws_vpc.requester.id})"
  }

  count = "${var.enable ? 1 : 0}"
}

output "peering" {
  value = {
    id = "${aws_vpc_peering_connection.peer.id}"
  }
}
