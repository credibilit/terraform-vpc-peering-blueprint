// Requester (local)
data "aws_vpc" "requester" {
  id = "${var.vpc_id}"
}

// Accepter
data "aws_vpc" "accepter" {
  id = "${var.peer_vpc_id}"
}
