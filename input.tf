variable "account" {}


// Requester VPC
variable "vpc_id" {
  description = "The ID of the requester VPC"
}

variable "vpc_route_tables" {
  type = "list"
  description = "List of ID of routing tables to add routes to vpc peering conn"
  default = []
}

variable "vpc_route_table_count" {
  description = "Count of vpc_route_tables"
  default = 0
}


// Accepter VPC
variable "peer_vpc_id" {
  description = "The ID of the VPC with which you are creating the VPC Peering Connection"
}

variable "peer_route_tables" {
  type = "list"
  description = "List of ID of routing tables to add routes to vpc peering conn"
  default = []
}

variable "peer_route_table_count" {
  description = "Count of peer_route_tables"
  default = 0
}

variable "enable" {
  description = "Boolean enable/disable resources creation"
  default = true
}
