// VPC Requester
resource "aws_vpc" "requester" {
  cidr_block            = "10.10.0.0/16"
  enable_dns_support    = true
  enable_dns_hostnames  = true
  tags {
    Name = "requester"
  }
}

resource "aws_route_table" "r_prv_a" {
  vpc_id = "${aws_vpc.requester.id}"
  tags {
    Name = "req-prv-a"
  }
}

resource "aws_route_table" "r_prv_b" {
  vpc_id = "${aws_vpc.requester.id}"
  tags {
    Name = "req-prv-b"
  }
}

resource "aws_route_table" "r_pub" {
  vpc_id = "${aws_vpc.requester.id}"
  tags {
    Name = "req-pub"
  }
}


// VPC Accepter
resource "aws_vpc" "accepter" {
  cidr_block            = "10.20.0.0/16"
  enable_dns_support    = true
  enable_dns_hostnames  = true
  tags {
    Name = "accepter"
  }
}

resource "aws_route_table" "a_prv_a" {
  vpc_id = "${aws_vpc.accepter.id}"
  tags {
    Name = "acc-prv-a"
  }
}

resource "aws_route_table" "a_prv_b" {
  vpc_id = "${aws_vpc.accepter.id}"
  tags {
    Name = "acc-prv-b"
  }
}

resource "aws_route_table" "a_pub" {
  vpc_id = "${aws_vpc.accepter.id}"
  tags {
    Name = "acc-pub"
  }
}


// Module test
variable "account" {}

module "test" {
  source  = "../"
  account = "${var.account}"

  // vpcs
  vpc_id      = "${aws_vpc.requester.id}" 
  peer_vpc_id = "${aws_vpc.accepter.id}"

  // routes vpc requester to accepter
  vpc_route_tables      = "${list(
    aws_route_table.r_prv_a.id,
    aws_route_table.r_prv_b.id,
    aws_route_table.r_pub.id
  )}"
  vpc_route_table_count = 3

  // routes vpc accepter to requester
  peer_route_tables       = "${list(
    aws_route_table.a_prv_a.id,
    aws_route_table.a_prv_b.id,
    aws_route_table.a_pub.id
  )}"
  peer_route_table_count  = 3
}

output "peering" {
  value = "${module.test.peering}"
}

output "routes_in_requester_to_accepter" {
  value = "${module.test.routes_in_requester_to_accepter}"
}

output "routes_in_accepter_to_requester" {
  value = "${module.test.routes_in_accepter_to_requester}"
}

module "test_disable" {
  source  = "../"
  account = "${var.account}"

  // vpcs
  vpc_id      = "${aws_vpc.requester.id}" 
  peer_vpc_id = "${aws_vpc.accepter.id}"

  // routes vpc requester to accepter
  vpc_route_tables      = "${list(
    aws_route_table.r_prv_a.id,
    aws_route_table.r_prv_b.id,
    aws_route_table.r_pub.id
  )}"
  vpc_route_table_count = 3

  // routes vpc accepter to requester
  peer_route_tables       = "${list(
    aws_route_table.a_prv_a.id,
    aws_route_table.a_prv_b.id,
    aws_route_table.a_pub.id
  )}"
  peer_route_table_count  = 3

  enable = false
}
